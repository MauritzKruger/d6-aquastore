# d6 - AquaStore

Carreer challenge set by d6.

# Technology Used
- SailsJS Framework build on NodeJS (ExpressJS)

# Launching & Seeding of API & Database
- First add MySQL database details within config/local.js file
`datastores: { default: { adapter: 'sails-mysql', url: 'mysql://<username>:<password>@localhost:3306/<databaseName>'}},`

- After MySQL database details have been added, run the following commands within root folder of project
`npm i` then `npm run seed`. This will seed the database with initial data

- Next lift the api by running `node app.js`

# Once API is Running, using any API tester (Postman/Talent API Tester), the following routes can be used
- GET http://localhost:8080/Aquarium will render a list of all aquariums
- GET http://localhost:8080/Fish will render a list of all fish
- POST http://localhost:8080/Aquarium/getFishForAquarium with JSON object `{aquariumId: 10}` will render a list of all fish within specified aquirium
- POST http://localhost:8080/Fish/addFishToTank with JSON object `{aquariumId: 10, fishId: 4}` will add fish to aquarium if rules are met
- POST http://localhost:8080/Fish/save with JSON object `{id: null/number, species: 'text', color: 'text', numberOfFins: number}` will create/update fish in database if validation passes

