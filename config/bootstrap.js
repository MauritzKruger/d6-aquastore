/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */
const fs = require(`fs`);

module.exports.bootstrap = function (cb) {
  setTimeout(
    async function () {
      if (sails.config.seed) {
        sails.log.info(`Checking Models :: Populate Data`);

        let mainModelsWithData = [];
        let mainModelsWithoutData = [];
        let associationModelsWithData = [];
        let associationModelsWithoutData = [];
        const checkIfDataExists = (folder, model) => {
          return new Promise((resolve, reject) => {
            fs.exists(`./api/data/${folder}/${sails.models[model].globalId}.js`, (exists) => {
              if (exists) {
                if (folder === 'associations') {
                  associationModelsWithData.push(model);
                } else {
                  mainModelsWithData.push(model);
                }
              } else {
                if (sails.models[model].globalId) {
                  if (folder === 'associations') {
                    associationModelsWithoutData.push(sails.models[model].globalId);
                  } else {
                    mainModelsWithoutData.push(sails.models[model].globalId);
                  }
                }
              }

              resolve();
            });
          });
        };

        let checkPromises = [];

        (Object.keys(sails.config.datastores))
          .forEach((datastore) => {
            let dataStoreName = datastore.charAt(0).toUpperCase() + datastore.substring(1);
            sails.log(`Database Data Store ::`, dataStoreName);

            sails.log(`Database Adapter    ::`, sails.config.datastores[datastore].adapter);
            console.log();

            sails.log.info(`Gathering Data for Model Population...`);

            for (let model in sails.models) {
              if ((sails.models[model]).datastore === datastore) {
                // Check that model has seed data
                checkPromises.push(checkIfDataExists((sails.models[model]).dataFolder, (model).toLowerCase()));
              }
            }
          });

        // Promise.all
        Promise.all(checkPromises)
          .then(async (res) => {
            let modelsWithoutData = [].concat(mainModelsWithoutData).concat(associationModelsWithoutData);
            if (modelsWithoutData.length > 0) {
              modelsWithoutData = modelsWithoutData
                .sort((a, b) => (a.name > b.name) ? 1 : -1)
                .sort((a, b) => (a.type > b.type) ? 1 : -1);

              console.log(``);

              sails.log.info(`Model(s) without Seed Data:`);
              modelsWithoutData.forEach((model) => {
                sails.log.debug(`-- ${model}`);
              });

              console.log(``);
            }

            let modelsWithData = [].concat(mainModelsWithData.sort((a, b) => (a > b) ? 1 : -1))
              .concat(associationModelsWithData.sort((a, b) => (a > b) ? 1 : -1));
            if (modelsWithData.length > 0) {
              sails.log.info(`Seed Data Processes Started For:`);

              sails.models[modelsWithData[0]].populateModels(modelsWithData)
                .then(function () {
                  console.log(``);
                  sails.log.info(`Seed Data Process Completed`);
                  process.exit(0);
                })
                .catch(function (err) {
                  sails.log.error(`-- An Error Occurred... `, err);
                  process.exit(1);
                });
            } else {
              sails.log.info(`Not populating model(s), lifting Sails...`);
              return cb();
            }
          });
      } else {
        // It`s very important to trigger this cb method when you are finished
        // with the bootstrap!  (otherwise your server will never lift, since it`s waiting on the bootstrap)
        sails.log.info(`Not seeding, lifting Sails...`);
        return cb();
      }
    }, 5000);
};
