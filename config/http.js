/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For more information on configuration, check out:
 * https://sailsjs.com/config/http
 */

module.exports.http = {

  /****************************************************************************
  *                                                                           *
  * Sails/Express middleware to run for every HTTP request.                   *
  * (Only applies to HTTP requests -- not virtual WebSocket requests.)        *
  *                                                                           *
  * https://sailsjs.com/documentation/concepts/middleware                     *
  *                                                                           *
  ****************************************************************************/

  middleware: {

    /***************************************************************************
    *                                                                          *
    * The order in which middleware should be run for HTTP requests.           *
    * (This Sails app`s routes are handled by the `router` middleware below.)  *
    *                                                                          *
    ***************************************************************************/

    order: [
      `cookieParser`,
      `session`,
      `bodyParser`,
      `compress`,
      `poweredBy`,
      `$custom`,
      `router`,
      `www`,
      `favicon`,
      `jwtErrorCheck`
    ],

    // An example of a custom HTTP middleware function:
    jwtErrorCheck: (function() {
      return function(err, req, res, next) {
        if (err.status === 401) {
          return res.status(401).send(err);
        }
      };
    })(),

    /***************************************************************************
    *                                                                          *
    * The body parser that will handle incoming multipart HTTP requests.       *
    *                                                                          *
    * https://sailsjs.com/config/http#?customizing-the-body-parser             *
    *                                                                          *
    ***************************************************************************/

    bodyParser: require(`skipper`)({
      // extended: true,
      limit: `100mb`,
      // maxTimeToBuffer: 10000,
      // maxTimeToWaitForFirstFile: 4500,
      // maxWaitTimeBeforePassingControlToApp: 1000,
      strict: true
      // onBodyParserError: function(err, req, res, next) {
      //   debugger;
      //   sails.log.error(`Body Parser Error ::`, err);
      //   return next(err);
      // }
    }),

    compress: require(`compression`)()
  },

  trustProxy: true

};
