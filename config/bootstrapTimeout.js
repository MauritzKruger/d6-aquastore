/**
 * Bootstrap Timeout
 *
 * Setting the bootstrap timeout warning for config/bootstrap.js which
 * executes seeding data as well.
 */
module.exports.bootstrapTimeout = 60000;
