/**
 * Blueprint API Configuration
 * (sails.config.blueprints)
 *
 * For background on the blueprint API in Sails, check out:
 * https://sailsjs.com/docs/reference/blueprint-api
 *
 * For details and more available options, see:
 * https://sailsjs.com/config/blueprints
 */

module.exports.blueprints = {

  /***************************************************************************
  *                                                                          *
  * Automatically expose implicit routes for every action in your app?       *
  *                                                                          *
  ***************************************************************************/

  actions: true,


  /***************************************************************************
  *                                                                          *
  * Automatically expose RESTful routes for your models?                     *
  *                                                                          *
  ***************************************************************************/

  rest: true,


  /***************************************************************************
  *                                                                          *
  * Automatically expose CRUD `shortcut` routes to GET requests?             *
  * (These are enabled by default in development only.)                      *
  *                                                                          *
  ***************************************************************************/

  shortcuts: true,

  /**
   * Default Limit When Retrieving Data
   */
  parseBlueprintOptions: (req) => {
    // Get the default query options.
    let queryOptions = req._sails.hooks.blueprints.parseBlueprintOptions(req);

    if ((queryOptions).hasOwnProperty(`criteria`)) {
      if ((queryOptions.criteria).hasOwnProperty(`sort`)) {
        if (!Array.isArray(queryOptions.criteria.sort)) {
          const sortCriteria = [];
          (Object.keys(queryOptions.criteria.sort)).forEach((key) => {
            let object = {};
            object[key] = queryOptions.criteria.sort[key];
            sortCriteria.push(object);
          });
          queryOptions.criteria.sort = sortCriteria;
        }
      }
    }

    if (req.options.blueprintAction === `find` || req.options.blueprintAction === `populate`) {
      if ((req.allParams()).hasOwnProperty(`limit`) !== true) {
        queryOptions.criteria.limit = 10000000;
      }
    }

    return queryOptions;
  }

};
