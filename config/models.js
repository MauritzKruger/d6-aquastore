/**
 * Default model settings
 * (sails.config.models)
 *
 * Your default, project-wide model settings. Can also be overridden on a
 * per-model basis by setting a top-level properties in the model definition.
 *
 * For details about all available model settings, see:
 * https://sailsjs.com/config/models
 *
 * For more general background on Sails model settings, and how to configure
 * them on a project-wide or per-model basis, see:
 * https://sailsjs.com/docs/concepts/models-and-orm/model-settings
 */

/**•
 * Function(s) to Populated Database Table(s)
 */
function truncateModel(model) {
  return new Promise(
    (resolve, reject) => {
      sails.log.debug(`- Attempting Truncation...`);
      (sails.models[model]).destroy({}).exec(
        (err, res) => {
          if (err) {
            sails.log.error(`-- An Error Occurred ::`, err);
            reject(err);
          } else {
            sails.log.info(`-- Model Truncated`);
            resolve();
          }
        });
    });
};

function populateArray(model, data) {
  return new Promise(
    (resolve, reject) => {
      sails.log.debug(`- Populating Model with Array...`);

      model.createEach(data).exec(
        (err) => {
          if (err) {
            sails.log.error(`-- An Error Occurred while Populating Array ::`, err);
            reject(err);
          } else {
            sails.log.info(`-- Model Populated with Array`);
            resolve();
          }
        });
    });
};

function populateObject(model, data) {
  return new Promise(
    (resolve, reject) => {
      sails.log.debug(`- Populating Model with Object...`);

      model.create(data).exec(
        (err) => {
          if (err) {
            sails.log.error(`-- An Error Occurred while Populating Object ::`, err);
            reject(err);
          } else {
            sails.log.info(`-- Model Populated with Object`);
            resolve();
          }
        });
    });
};

function populate(model) {
  return new Promise(
    (resolve, reject) => {
      (sails.models[model]).count().exec(
        (err, res) => {
          if (!err && (res === 0 || res === undefined)) {
            // Get Model Data
            let data = require(`../api/data/${sails.models[model].dataFolder}/${sails.models[model].globalId}.js`).getData();

            if (data instanceof Promise) {
              data.then((response) => {
                if (response instanceof Array) {
                  resolve(populateArray(sails.models[model], response));
                } else {
                  resolve(populateObject(sails.models[model], response));
                }
              });
            } else {
              if (data instanceof Array) {
                resolve(populateArray(sails.models[model], data));
              } else {
                resolve(populateObject(sails.models[model], data));
              }
            }

          } else {
            sails.log.info(`Model has data, no need to populate`);
            resolve();
          }
        });
    });
};

function populateModel(models, index) {
  return new Promise(
    (resolve, reject) => {
      if (index < models.length) {
        let model = sails.models[models[index]];
        sails.log.info(`Model :: ${model.globalId}`);

        truncateModel(models[index])
          .then(() => {
            populate(models[index])
              .then(() => {
                resolve(populateModel(models, index + 1));
              })
              .catch((err) => {
                reject(err);
              });
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve();
      }
    });
};

module.exports.models = {


  /***************************************************************************
  *                                                                          *
  * Whether model methods like `.create()` and `.update()` should ignore     *
  * (and refuse to persist) unrecognized data-- i.e. properties other than   *
  * those explicitly defined by attributes in the model definition.          *
  *                                                                          *
  * To ease future maintenance of your code base, it is usually a good idea  *
  * to set this to `true`.                                                   *
  *                                                                          *
  * > Note that `schema: false` is not supported by every database.          *
  * > For example, if you are using a SQL database, then relevant models     *
  * > are always effectively `schema: true`.  And if no `schema` setting is  *
  * > provided whatsoever, the behavior is left up to the database adapter.  *
  * >                                                                        *
  * > For more info, see:                                                    *
  * > https://sailsjs.com/docs/concepts/orm/model-settings#?schema           *
  *                                                                          *
  ***************************************************************************/

  // schema: true,


  /***************************************************************************
  *                                                                          *
  * How and whether Sails will attempt to automatically rebuild the          *
  * tables/collections/etc. in your schema.                                  *
  *                                                                          *
  * > Note that, when running in a production environment, this will be      *
  * > automatically set to `migrate: `safe``, no matter what you configure   *
  * > here.  This is a failsafe to prevent Sails from accidentally running   *
  * > auto-migrations on your production database.                           *
  * >                                                                        *
  * > For more info, see:                                                    *
  * > https://sailsjs.com/docs/concepts/orm/model-settings#?migrate          *
  *                                                                          *
  ***************************************************************************/

  // migrate: `alter`,


  /***************************************************************************
  *                                                                          *
  * Base attributes that are included in all of your models by default.      *
  * By convention, this is your primary key attribute (`id`), as well as two *
  * other timestamp attributes for tracking when records were last created   *
  * or updated.                                                              *
  *                                                                          *
  * > For more info, see:                                                    *
  * > https://sailsjs.com/docs/concepts/orm/model-settings#?attributes       *
  *                                                                          *
  ***************************************************************************/

  attributes: {
    // createdAt: { type: `number`, autoCreatedAt: true, },
    // updatedAt: { type: `number`, autoUpdatedAt: true, },
    // Mongo Database
    // id: { type: `string`, columnName: `_id`},
    // MySQL or MSSQL Database
    // id: { type: `number`, columnType: `integer`, autoIncrement: true },
  },

  archiveModelIdentity: false,

  beforeCreate: function (values, next) {
    if (values.hasOwnProperty(`createdAt`)) {
      values.createdAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);
    }
    if (values.hasOwnProperty(`updatedAt`)) {
      values.updatedAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);
    }
    next();
  },

  beforeUpdate: function (values, next) {
    if (values.hasOwnProperty(`id`)) {
      delete values.id
    }
    if (values.hasOwnProperty(`updatedAt`)) {
      values.updatedAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);
    }
    next();
  },

  customToJSON: function () {
    return _.omit(this, [`salt`, `password`]);
  },


  /******************************************************************************
  *                                                                             *
  * The set of DEKs (data encryption keys) for at-rest encryption.              *
  * i.e. when encrypting/decrypting data for attributes with `encrypt: true`.   *
  *                                                                             *
  * > The `default` DEK is used for all new encryptions, but multiple DEKs      *
  * > can be configured to allow for key rotation.  In production, be sure to   *
  * > manage these keys like you would any other sensitive credential.          *
  *                                                                             *
  * > For more info, see:                                                       *
  * > https://sailsjs.com/docs/concepts/orm/model-settings#?dataEncryptionKeys  *
  *                                                                             *
  ******************************************************************************/

  // dataEncryptionKeys: {
  //   default: `Y0X33zEAClmt2Z0wSE1PI30SHto09IPjTQSVoK13AHI=`
  // },


  /***************************************************************************
  *                                                                          *
  * Whether or not implicit records for associations should be cleaned up    *
  * automatically using the built-in polyfill.  This is especially useful    *
  * during development with sails-disk.                                      *
  *                                                                          *
  * Depending on which databases you`re using, you may want to disable this  *
  * polyfill in your production environment.                                 *
  *                                                                          *
  * (For production configuration, see `config/env/production.js`.)          *
  *                                                                          *
  ***************************************************************************/

  // cascadeOnDestroy: true


  /***************************************************************************
  *                                                                          *
  * Seeds database table with dataset for testing purposes.                  *
  *                                                                          *
  * To use this functionality, add `seedData` in your model and call the     *
  * method in the bootstrap.js file.                                         *
  *                                                                          *
  ***************************************************************************/

  populateModels: (models) => {
    return new Promise(
      (resolve, reject) => {
        populateModel(models, 0)
          .then(() => {
            resolve();
          })
          .catch((err) => {
            reject(err);
          });
      });
  }
};
