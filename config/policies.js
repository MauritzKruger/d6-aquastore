/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,

  /***************************************************************************
   * Policy List
   *
   * isAuthorized    - Checks to see if user if authorized on the API
   * decryptPackage  - Checks to see if request data is encrypted then
   *                   decrypts data for backend to use
   * isUnique        - Checks to see if request data is unique for model then
   *                   either allows or rejects request
   ***************************************************************************/
  '*': true,

};
