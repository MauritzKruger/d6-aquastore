/**
 * Model
 *
 * @help  :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	datastore: `default`,
  dataFolder: `associations`,
	globalId: `AquariumFishAssociation`,
	tableName: `AquariumFishAssociation`,
	schema: true,
	attributes: {
		id: {
			type: `number`,
			columnType: `integer`,
			autoIncrement: true
		},
    aquariumId: {type: `number`, columnType: `integer`},
    fishId: {type: `number`, columnType: `integer`},
		createdAt: {type: `string`, columnType: `dateTime`, autoCreatedAt: true},
		updatedAt: {type: `string`, columnType: `dateTime`, autoUpdatedAt: true}
	}
};
