/**
 * Model
 *
 * @help  :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	datastore: `default`,
  dataFolder: `main`,
	globalId: `Aquarium`,
	tableName: `Aquariums`,
	schema: true,
	attributes: {
		id: {
			type: `number`,
			columnType: `integer`,
			autoIncrement: true
		},
		glassType: {type: `string`},
		liters: {type: `number`, columnType: `double`},
    gallons: {type: `number`, columnType: `double`},
		shape: {type: `string`},
		createdAt: {type: `string`, columnType: `dateTime`, autoCreatedAt: true},
		updatedAt: {type: `string`, columnType: `dateTime`, autoUpdatedAt: true}
	},

	beforeCreate: function (values, proceed) {
		if (values.liters) {
			// Convert Liters to Gallons
			values.gallons = Number((Number(values.liters) * 0.264172).toFixed(6));
		} else if (values.gallons) {
			// Convert Gallons to Liters
			values.liters = Number((Number(values.gallons) * 3.78541).toFixed(6));
		}

		// Set date for Audit
		values.createdAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);
		values.updatedAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);

		return proceed();
	}
};
