/**
 * Model
 *
 * @help  :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	datastore: `default`,
  dataFolder: `main`,
	globalId: `Fish`,
	tableName: `Fish`,
	schema: true,
	attributes: {
		id: {
			type: `number`,
			columnType: `integer`,
			autoIncrement: true
		},
		species: {type: `string`},
		color: {type: `string`},
    numberOfFins: {type: `number`, columnType: `integer`},
    createdAt: {type: `string`, columnType: `dateTime`, autoCreatedAt: true},
		updatedAt: {type: `string`, columnType: `dateTime`, autoUpdatedAt: true}
	},

	beforeCreate: function (values, proceed) {
		// Validate Value(s)


		// Set date for Audit
		values.createdAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);
		values.updatedAt = sails.config.globals.moment().utc().format(`YYYY-MM-DD HH:mm:ss`);

		return proceed();
	}
};
