'use strict';

let data = [{
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Rectangle/Square`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Male`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Guppy - Female`, color: `Various`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Radius Triangle`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 50, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 75, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `5mm`, liters: 100, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `6mm`, liters: 400, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 800, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `8mm`, liters: 1000, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `10mm`, liters: 1200, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Giant Danio`, color: `Black & Orange`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Goldfish`, color: `Orange`, numberOfFins: 5},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Zebrafish`, color: `Black & Silver`, numberOfFins: 3},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Glassfish`, color: `Various`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Blue`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Neon`, color: `Black`, numberOfFins: 4},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Orange`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `Black`, numberOfFins: 2},
  }, {
    aquariumId: {glassType: `12mm`, liters: 1600, shape: `Cylinder`},
    fishId: {species: `Molly`, color: `White`, numberOfFins: 2},
  }];

/**
 * Returns data
 * @return {Array} - array of objects
 */
async function getData() {
  // Retrieve Object Id(s) from Parent Table(s)
  for (const d of data) {
    const aquarium = await Aquarium.findOne(d.aquariumId);
    const fish = await Fish.findOne(d.fishId);

    d.aquariumId = aquarium.id;
    d.fishId = fish.id;
  }

  return data.slice();
}

module.exports = {
  getData: getData
};
