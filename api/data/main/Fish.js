'use strict';

let data = [{
  species: `Giant Danio`,
  color: `Black & Orange`,
  numberOfFins: 3
}, {
  species: `Guppy - Male`,
  color: `Various`,
  numberOfFins: 3
}, {
  species: `Guppy - Female`,
  color: `Various`,
  numberOfFins: 3
}, {
  species: `Goldfish`,
  color: `Orange`,
  numberOfFins: 5
}, {
  species: `Zebrafish`,
  color: `Black & Silver`,
  numberOfFins: 3
}, {
  species: `Glassfish`,
  color: `Various`,
  numberOfFins: 4
}, {
  species: `Neon`,
  color: `Blue`,
  numberOfFins: 4
}, {
  species: `Neon`,
  color: `Black`,
  numberOfFins: 4
}, {
  species: `Molly`,
  color: `Orange`,
  numberOfFins: 2
}, {
  species: `Molly`,
  color: `Black`,
  numberOfFins: 2
}, {
  species: `Molly`,
  color: `White`,
  numberOfFins: 2
}];

/**
 * Returns data
 * @return {Array} - array of objects
 */
async function getData() {
	return data.slice();
}

module.exports = {
	getData: getData
};
