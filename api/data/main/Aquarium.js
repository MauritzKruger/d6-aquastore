'use strict';

let data = [{
  glassType: `5mm`,
  liters: 50,
  shape: `Rectangle/Square`
}, {
  glassType: `5mm`,
  liters: 75,
  shape: `Rectangle/Square`
}, {
  glassType: `5mm`,
  liters: 100,
  shape: `Rectangle/Square`
}, {
  glassType: `6mm`,
  liters: 200,
  shape: `Rectangle/Square`
}, {
  glassType: `6mm`,
  liters: 400,
  shape: `Rectangle/Square`
}, {
  glassType: `8mm`,
  liters: 600,
  shape: `Rectangle/Square`
}, {
  glassType: `8mm`,
  liters: 800,
  shape: `Rectangle/Square`
}, {
  glassType: `8mm`,
  liters: 1000,
  shape: `Rectangle/Square`
}, {
  glassType: `10mm`,
  liters: 1200,
  shape: `Rectangle/Square`
}, {
  glassType: `12mm`,
  liters: 1600,
  shape: `Rectangle/Square`
}, {
  glassType: `5mm`,
  liters: 50,
  shape: `Radius Triangle`
}, {
  glassType: `5mm`,
  liters: 75,
  shape: `Radius Triangle`
}, {
  glassType: `5mm`,
  liters: 100,
  shape: `Radius Triangle`
}, {
  glassType: `6mm`,
  liters: 200,
  shape: `Radius Triangle`
}, {
  glassType: `6mm`,
  liters: 400,
  shape: `Radius Triangle`
}, {
  glassType: `8mm`,
  liters: 600,
  shape: `Radius Triangle`
}, {
  glassType: `8mm`,
  liters: 800,
  shape: `Radius Triangle`
}, {
  glassType: `8mm`,
  liters: 1000,
  shape: `Radius Triangle`
}, {
  glassType: `10mm`,
  liters: 1200,
  shape: `Radius Triangle`
}, {
  glassType: `12mm`,
  liters: 1600,
  shape: `Radius Triangle`
}, {
  glassType: `5mm`,
  liters: 50,
  shape: `Cylinder`
}, {
  glassType: `5mm`,
  liters: 75,
  shape: `Cylinder`
}, {
  glassType: `5mm`,
  liters: 100,
  shape: `Cylinder`
}, {
  glassType: `6mm`,
  liters: 200,
  shape: `Cylinder`
}, {
  glassType: `6mm`,
  liters: 400,
  shape: `Cylinder`
}, {
  glassType: `8mm`,
  liters: 600,
  shape: `Cylinder`
}, {
  glassType: `8mm`,
  liters: 800,
  shape: `Cylinder`
}, {
  glassType: `8mm`,
  liters: 1000,
  shape: `Cylinder`
}, {
  glassType: `10mm`,
  liters: 1200,
  shape: `Cylinder`
}, {
  glassType: `12mm`,
  liters: 1600,
  shape: `Cylinder`
}];

/**
 * Returns data
 * @return {Array} - array of objects
 */
async function getData() {
	return data.slice();
}

module.exports = {
	getData: getData
};
