/**
 * Controller
 *
 * @help  :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  getFishForAquarium: async (req, res) => {
    let parameters = {};

    try {
      parameters = JSON.parse(req.body);
    } catch (err) {
      parameters = req.body;
    }

    if (parameters.hasOwnProperty(`aquariumId`)) {
      // Check That Aquaruim Exists
      let aquarium = null;
      try {
        aquarium = await Aquarium.findOne({id: parameters.aquariumId});
      } catch (err) {
        sails.log.error(`Error retrieving \`Aquarium\` ::`, err);
        return res.status(500)
          .send({
            error: `Internal Server Error`,
            details: err
          });
      }

      if (!aquarium) {
        return res.status(404)
          .send({
            error: `Not Found - Aquarium [${parameters.aquariumId}] does not exist`
          });
      }

      // Aquarium Found, Get Fish In Tank
      let fishInAquarium = [];
      try {
        fishInAquarium = await sails.sendNativeQuery(`SELECT
  f.species AS species
  , f.color AS color
  , f.numberOfFins AS numberOfFins
FROM
  AquariumFishAssociation afc
INNER JOIN
  Fish f ON afc.fishId = f.id
WHERE
  afc.aquariumId = ${parameters.aquariumId}
ORDER BY
  f.species`);

        if (fishInAquarium.hasOwnProperty(`rows`)) {
          fishInAquarium = fishInAquarium.rows;
        }
      } catch (err) {
        sails.log.error(`Error retrieving \`Fish in Aquarium\` ::`, err);
        return res.status(500)
          .send({
            error: `Internal Server Error`,
            details: err
          });
      }

      return res.status(200).send(fishInAquarium);
    } else {
      return res.status(400)
        .send({
          error: `Invalid Request - Missing Parameter(s)`
        });
    }
  }
};
