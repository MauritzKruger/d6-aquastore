/**
 * Controller
 *
 * @help  :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  save: async (req, res) => {
    let parameters = {};

    try {
      parameters = JSON.parse(req.body);
    } catch (err) {
      parameters = req.body;
    }

    // Validate Parameter(s)
    // Check That species, color and number of fins are entered
    let id = (parameters.id) ? parameters.id : null;
    let species = (parameters.species) ? `${parameters.species}` : ``;
    let color = (parameters.color) ? `${parameters.color}` : ``;
    let numberOfFins = (parameters.numberOfFins) ? parameters.numberOfFins : 0;

    // Glass Type and Shape should be entered
    if (species.length === 0) {
      return res.status(400)
        .send({
          error: `Invalid Request - Species should be specified`
        });
    }

    if (color.length === 0) {
      return res.status(400)
        .send({
          error: `Invalid Request - Color should be specified`
        });
    }

    // Number of Fins should be Numberic
    if (!(/^\d$/).test(numberOfFins)) {
      return res.status(400)
        .send({
          error: `Invalid Request - Number of fins should be a numeric value`
        });
    }

    // As all tests passed, fish species can be added/updated in table `Fish`
    let response = null;
    try {
      if (id) {
        response = await Fish.update({id}).set({species, color, numberOfFins}).fetch();
      } else {
        response = await Fish.create({species, color, numberOfFins}).fetch();
      }
    } catch (err) {
      sails.log.error(`Error inserting/updating  \`Fish\` ::`, err);
      return res.status(500)
        .send({
          error: `Internal Server Error`,
          details: err
        });
    }

    return res.status(200).send(response);
  },
  addFishToTank: async (req, res) => {
    let parameters = {};

    try {
      parameters = JSON.parse(req.body);
    } catch (err) {
      parameters = req.body;
    }

    if (parameters.hasOwnProperty(`aquariumId`) && parameters.hasOwnProperty(`fishId`)) {
      let aquariumId = (parameters.aquariumId) ? parameters.aquariumId : 0;
      let fishId = (parameters.fishId) ? parameters.fishId : 0;

      if (Number(aquariumId) <= 0 || isNaN(aquariumId)) {
        return res.status(400)
          .send({
            error: `Invalid Request - Aquarium Id should be specified`
          });
      }

      if (Number(fishId) <= 0 || isNaN(fishId)) {
        return res.status(400)
          .send({
            error: `Invalid Request - Fish Id should be specified`
          });
      }

      // Retrieve Aquarium Details
      let aquarium = null;
      try {
        aquarium = await Aquarium.find({id: aquariumId});
      } catch (err) {
        sails.log.error(`Error retrieving \`Aquarium\` ::`, err);
        return res.status(500)
          .send({
            error: `Internal Server Error`,
            details: err
          });
      }

      if (!aquarium) {
        return res.status(404)
          .send({
            error: `Not Found - Aquarium not found`
          });
      }

      // Retrieve Fish Details
      let fish = null;
      try {
        fish = await Fish.find({id: fishId});
      } catch (err) {
        sails.log.error(`Error retrieving \`Fish\` ::`, err);
        return res.status(500)
          .send({
            error: `Internal Server Error`,
            details: err
          });
      }

      if (!fish) {
        return res.status(404)
          .send({
            error: `Not Found - Fish not found`
          });
      }

      // Check That Tank is Large Enough For Fish
      if (Number(fish[0].numberOfFins) >= 3 && Number(aquarium[0].liters) <= 75) {
        return res.status(403)
          .send({
            error: `Forbidden - Fish with 3 fins or more cannot go into Aquariums of 75 liters or less`
          });
      }

      // Check If Fish is Guppy or Goldfish that Aquarium does not contain the other
      const otherFish = (((fish[0].species).toLowerCase()).indexOf(`goldfish`) !== -1) ?
        `AND (f.species LIKE '%Guppy%' OR f.species LIKE '%Guppies%')` :
        `AND f.species LIKE '%Goldfish%'`;
      let fishInTank = [];
      try {
        fishInTank = await sails.sendNativeQuery(`SELECT
  f.species AS species
FROM
  AquariumFishAssociation afc
INNER JOIN
  Fish f ON afc.fishId = f.id
WHERE
  afc.aquariumId = ${aquarium[0].id}
  ${otherFish}
`);

        if (fishInTank.hasOwnProperty(`rows`)) {
          fishInTank = fishInTank.rows;
        }
      } catch (err) {
        sails.log.error(`Error retrieving \`Fish in Aquarium\` ::`, err);
      }

      // Tank contains the other fish species, send error message to user
      if (fishInTank.length > 0) {
        return res.status(403)
          .send({
            error: `Forbidden - Guppies & Goldfish cannot go into the same Aquarium`
          });
      }

      // All Rule(s) Passed, add Fish to Aquarium
      let response = null;
      try {
        response = await AquariumFishAssociation.create({
          aquariumId: aquarium[0].id,
          fishId: fish[0].id
        }).fetch();
      } catch (err) {
        sails.log.error(`Error creating \`AquariumFishAssociation\` ::`, err);
        return res.status(500)
          .send({
            error: `Internal Server Error`,
            details: err
          });
      }

      return res.status(200).send(response);
    } else {
      return res.status(400)
        .send({
          error: `Invalid Request - Missing Parameter(s)`
        });
    }
  }
};
